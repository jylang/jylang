// Autogenerated AST node
package org.python.antlr.op;

import org.python.antlr.base.cmpop;
import org.python.antlr.PythonTree;
import org.python.core.Py;
import org.python.core.PyObject;
import org.python.core.PyUnicode;
import org.python.core.PyType;
import org.python.annotations.ExposedGet;
import org.python.annotations.ExposedMethod;
import org.python.annotations.ExposedNew;
import org.python.annotations.ExposedType;

@ExposedType(name = "_ast.Gt", base = cmpop.class)
public class Gt extends PythonTree {
    public static final PyType TYPE = PyType.fromClass(Gt.class);

    public Gt() {
        super(TYPE);
    }
    public Gt(PyType subType) {
        super(subType);
    }
    @ExposedNew
    @ExposedMethod
    public void Gt___init__(PyObject[] args, String[] keywords) {}

    private final static PyUnicode[] fields = new PyUnicode[0];
    @ExposedGet(name = "_fields")
    public PyUnicode[] get_fields() { return fields; }

    private final static PyUnicode[] attributes = new PyUnicode[0];
    @ExposedGet(name = "_attributes")
    public PyUnicode[] get_attributes() { return attributes; }

    @ExposedMethod
    public PyObject __int__() {
        return Gt___int__();
    }

    final PyObject Gt___int__() {
        return Py.newInteger(5);
    }

    @Override
    public String toStringTree() {
        return Gt.class.toString();
    }
}
